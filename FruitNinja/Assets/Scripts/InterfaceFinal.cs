﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour
{
    public Text textThrown;
    public Text textDestroyed;
    public Text textBombs;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.currentNumberStonesThrown--;
    }

    // Update is called once per frame
    void Update()
    {
        textThrown.text = "Number of Stones: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Destroyed: " + GameManager.currentNumberDestroyedStones;
        textBombs.text = "Bombs: " + GameManager.currentNumberBombs;
    }

    public void Click()
    {
        Application.LoadLevel("Awake");
    }
}